var page = {
	init: function(){
    this.slide();
  },

  slide: function() {
    $(".produto .owl-carousel").owlCarousel({
      loop: true,
      dots: true,
      nav: false,
      navSpeed: 2000,
      autoplay: true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        992: {
          items: 3
        },
        1440: {
          items: 4
        }
      }
    });
  }
}   
page.init();