<!-- footer -->
  <footer class="footer">
      <div class="container">
        <div class="logo_loja col-xs-12 col-md-3">
          <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo_footer.png" alt="Imagem contém logo marca da <?php echo bloginfo( 'name' ); ?>"></a>
        </div>
        <div class="info_horario col-xs-12 col-sm-6 col-md-3">
          <span class="info_head">Horário de Atendimento</span>
          <span class="info_subhead">Segunda a Sexta 08:00 as 18:00h</span>
          <span class="info_subhead">Sábado: 08:00 as 12:00h</span>
        </div>
        <div class="info_endereco col-xs-12 col-sm-6  col-md-3">
          <span class="info_head">Endereço</span>
          <span class="info_subhead">BRASÍLIA: R: C 5 - LOTE 06 - LOJA 2A - TAGUATINGA - CENTRO </span>
        </div>

        <div class="info_contato col-xs-12 col-md-3">
          <span class="contato_head">Contato</span>
          <button class="btn-phone">
            <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
            <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
          </button>  
        </div>  
      </div>
      <div class="logo-footer">
        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo-moura.png" alt="Imagem contém logomarca da Moura"></a>
      </div>
  </footer>
<!-- /footer -->

<!-- JAVASCRIPT -->
<?php
  wp_enqueue_script( 'scripts', get_bloginfo( 'template_directory' ) . '/dist/js/scripts.js', array( 'jquery' ), '', true );
?>

<?php wp_footer(); ?>

<!-- analytics -->
<script>
  (function (f, i, r, e, s, h, l) {
    i['GoogleAnalyticsObject'] = s;
    f[s] = f[s] || function () {
        (f[s].q = f[s].q || []).push(arguments)
      }, f[s].l = 1 * new Date();
    h = i.createElement(r),
      l = i.getElementsByTagName(r)[0];
    h.async = 1;
    h.src = e;
    l.parentNode.insertBefore(h, l)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
  ga('send', 'pageview');
</script>

</body>
</html>
