<?php 
	get_header(); 
	//Template name: Home
?>
		<section class="produtos">
			<div class="container">
				<h2 class="title">ESCOLHA SEU TIPO DE BATERIA</h2>
				<div class="produto">
					<div class="owl-carousel">
						<div class="item">
							<div class="bg-produto" style="background-image: url('<?php echo get_template_directory_uri();?>/dist/images/item_1.png');"></div>
							<h3 class="produto_head">Bateria Automotiva</h3>
							<span class="produto_info">A bateria para carro é essencial para o funcionamento do veículo, principalmente com a modernização dos veículos. Quanto mais modernos, mais equipamentos e benefícios elétricos o veículo tem.
							</span>
							<button class="btn-phone">
					          <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
					          <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
					        </button>
						</div>

						<div class="item">
							<div class="bg-produto" style="background-image: url('<?php echo get_template_directory_uri();?>/dist/images/item_2.png');"></div>
							<h3 class="produto_head">Bateria Automotiva</h3>
							<span class="produto_info">A bateria para carro é essencial para o funcionamento do veículo, principalmente com a modernização dos veículos. Quanto mais modernos, mais equipamentos e benefícios elétricos o veículo tem.
							</span>
							<button class="btn-phone">
					          <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
					          <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
					        </button>
						</div>

						<div class="item">
							<div class="bg-produto" style="background-image: url('<?php echo get_template_directory_uri();?>/dist/images/item_3.png');"></div>
							<h3 class="produto_head">Bateria Automotiva</h3>
							<span class="produto_info">A bateria para carro é essencial para o funcionamento do veículo, principalmente com a modernização dos veículos. Quanto mais modernos, mais equipamentos e benefícios elétricos o veículo tem.
							</span>
							<button class="btn-phone">
					          <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
					          <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
					        </button>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- localização -->
		<section class="location">
		  <iframe src="https://snazzymaps.com/embed/98643" width="100%" height="100%" style="border:none;"></iframe>    
		</section>

	</main>


<?php get_footer(); ?>
