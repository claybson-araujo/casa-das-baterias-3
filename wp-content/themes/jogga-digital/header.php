<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
		  echo ' :';
	  } ?><?php bloginfo( 'name' ); ?></title>

  <link href="//www.google-analytics.com" rel="dns-prefetch">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="shortcut icon">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="apple-touch-icon-precomposed">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<?php wp_head(); ?>
  

</head>
<body <?php body_class(); ?>>
<!-- wrapper -->

  <!-- cabeçalho -->
  <header class="header">
    <div class="container">
      <div class="row">
        <!-- <div class="logo_header col-xs-6 col-sm-4">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/logo-moura.png" alt="">
        </div> -->

        <div class="logo__header col-xs-12 col-sm-offset-4 col-sm-4  ">
          <img src="<?php echo get_template_directory_uri();?>/dist/images/logo-loja.png" alt="">
        </div>
        
        <div class="phone_header col-sm-4">
          <button class="btn-phone">
            <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
            <a href="8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>

          </button>
        </div>
      </div>
    </div>
  </header>

  <main class="content">

    <!-- destaque -->
    <section class="featured">      
      <div class="container">
        <button class="btn-phone">
          <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
          <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
        </button>
        <div class="image-featured col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-6" style="background-image: url('<?php echo get_template_directory_uri();?>/dist/images/featured.png');"></div>
        <div>
          <h3 class="featured_head">Entregamos Agora Sua Bateria</h3>
          <h4 class="featured_subhead">Segunda a Sexta das 08:00 às 19:00</h4>
          <h4 class="featured_subhead">Sábado das 08:00 às 12:00</h4>
          <button class="btn-phone btn-featured">
            <a href="tel:8221267231" class="chamada-desktop"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Peça agora<br>(82) 2126-7231</a>
            <a href="tel:8221267231" class="chamada-mobile"><img src="<?php echo get_template_directory_uri();?>/dist/images/phone.png">Clique aqui e peça<br>agora (82) 2126-7231</a>
          </button>
        </div>
      </div>
    </section>
